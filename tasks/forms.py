from django import forms
from .models import Task


class DateTime(forms.DateTimeInput):
    input_type = "date"


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ("is_completed",)

        widgets = {
            "start_date": DateTime(),
            "due_date": DateTime(),
        }
